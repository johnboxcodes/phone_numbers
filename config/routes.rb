Rails.application.routes.draw do
  match '/phone_number' => 'phone_number#create', via: :post
  match '/' => 'phone_number#index', via: :get
end
