class PhoneNumberController < ApplicationController

  def index
  end
  def uk_pattern_phone_regex?(str)
    !!(str =~ /^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$/)
    rescue false
  end

  skip_before_action :verify_authenticity_token
  def phone_number_params
    params.require(:phone).permit(:phone_number)
  end

  def create
    data = JSON.parse(request.body.read)
    phone_number = data['phone'].to_s
    test_number = ""
    if phone_number[0..2] == "+44"
      test_number = phone_number
    else
      test_number = "+44" + phone_number
    end
    if uk_pattern_phone_regex?("#{test_number}")
      render json: data and return
    else
    render json: {error: "#{phone_number} not a valid uk number"}, status: 400 and return
    end
  end
end
