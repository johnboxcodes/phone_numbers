import React from 'react'
import { useCallback, useState, useEffect } from 'react';
import './PhoneNumberInput.css'

export const debounce = (fn, milis) => {
  let timeoutID
  return (...args) => {
    if (timeoutID) {
      clearTimeout(timeoutID);
    }
    timeoutID = setTimeout(() => {
      fn(...args)
    }, milis)
  };
};

const PhoneNumberInput = () => {
  const [phone, setPhone] = useState("")
  const [isValid, setIsValid] = useState(true)
  const validation = value => {
    let currentValue = value
    if (value[0] !== "+") {
      currentValue = `+44 ${currentValue}`
    }
    const valid = currentValue.match(phoneRegex)
    if (valid && valid.length) {
      setIsValid(true)
    } else {
      setIsValid(false)
    }
  }
  const debounceValidation = useCallback(debounce(validation, 600), []);
  const phoneRegex = /^(((\+44\s?\d{4}|\(?0\d{4}\)?)\s?\d{3}\s?\d{3})|((\+44\s?\d{3}|\(?0\d{3}\)?)\s?\d{3}\s?\d{4})|((\+44\s?\d{2}|\(?0\d{2}\)?)\s?\d{4}\s?\d{4}))(\s?\#(\d{4}|\d{3}))?$/mg;
  const submitPhoneNumber = async () => {
    console.log(phone)
    if (!phone) {
      return
    }
    let url = `http://localhost:3000/phone_number`

    const response = await fetch(
      url,
      {
        method: "POST",
        body: JSON.stringify({
          phone
        })
      }
    ).then(async (response) => {
      return response.json();
    })
    console.log(response)
  }

  const handleChange = value => {
    const currentValue = value.replace(/[a-zA-Z]+/g, "")
    setPhone(currentValue)
    debounceValidation(currentValue)
  }
  return (
    <div className="container">
      <h1>{isValid}</h1>
      <div className="checkout-input-wrapper checkout-input-wrapper--mobile-number">
        <input id="mobile-number"
          aria-labelledby="mobile-number-placeholder"
          autoComplete="tel"
          className="checkout-input"
          onChange={e => handleChange(e.target.value)}
          value={phone}
          name="mobile-number" type="tel" />
        <p className="checkout-placeholder mobile-number-placeholder" id="mobile-number-placeholder">Your mobile number
          <span className="optional-label">- optional</span>
        </p>
        <img alt="Valid input icon."
          className="checkout__checkmark"
          src="//d1qzwxap7t1x49.cloudfront.net/packs/media/checkmarks/green-checkmark-68ab5ccd.svg" />
      </div>
      <p className="courier-info">Your courier will text you on the day with a delivery time slot. This is optional.</p>
      {!isValid &&
        <p className="checkout-input__validation-error">Please enter a valid UK or IE mobile number.</p>
      }
      <button className="checkout__continue-button btn btn-updated btn-updated--blue"
        onClick={() => submitPhoneNumber()}
        type="button">SUBMIT PHONE NUMBER</button>
    </div>
  )
}

export default PhoneNumberInput
